<?php
$count = 10;
while ($count >= 0) {
    echo "$count<br>";
    --$count;
}
echo "<p>We have liftoff.</p>";
$count = 10;
do {
    echo "$count<br>";
    --$count;
} while ( $count >= 0);
echo "<p>...and again we have liftoff.</p>";
?>
<h1>for loop</h1>

<?php
for ($count = 10; $count > 0; $count--) {
    echo "$count<br>";
}
?>

<h1>foreach</h1>
<?php
$DaysOfWeek = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
foreach ($DaysOfWeek as $value) {
    echo "$value <br>";
}
?>