<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="menu.css">
    <script src="menu.js"></script>
    <title>menu.html</title>
</head>
<body onload="customMenu(); startHere();">
    <nav>
        <a href="index.php" class="menu" id="index">Home</a>
        <a href="story.php" class="menu" id="story">Bulldogs</a>
        <a href="contact.php" class="menu" id="form">Contact</a>
    </nav>
