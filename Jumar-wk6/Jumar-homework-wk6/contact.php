<?php
$name = $email = $subject = $message = '';

if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {
    if ( !empty( $_POST["firstName"] ) ) {
        $name = $_POST["firstName"];
    }
    if ( !empty( $_POST["email"] ) ) {
        $email = $_POST["email"];
    }
    if ( !empty( $_POST["subject"] ) ) {
        $subject = $_POST["subject"];
    }
    if ( !empty( $_POST["message"] ) ) {
        $message = $_POST["message"];
    }

}

$title = "Home";
include 'header.php';
?>

<div>
    <h2 style = "text-align:center">Contact Me</h2>
    <div id="success" class="inactive">Your message has been sent!</div>
    <form name="contact" novalidate action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" onsubmit="return checkForm()">
        <p>Your Name: <br>
        <input type="text" id="firstName" name="firstName" value="<?php echo $name; ?>" autofocus></p>

        <p>Your E-mail: <br>
        <input type="email" name="email" value="<?php echo $email; ?>" required></p>

        <p>Subject: <br>
        <input type="text" name="subject" value="<?php echo $subject; ?>"></p>

        <p>Message:<br >
        <textarea name="message" ><?php echo $message; ?></textarea></p>

        <p>Want to talk about different breeds?
        <ul id="extra">
            <!-- create extra li in here onclick -->
        </ul>
        <button type="button" class="add" onclick="buildExtraField()">Add breeds</button>
        <br><br>

    	<input type="checkbox" onclick="iAgree()" id="agreeCheckbox"> I agree

        <p><input type="button" onclick="clearForm(this)" value="Clear Form" >&nbsp; &nbsp;
        <input type="submit" id="submit" name="Submit" value="Send Form"></p>
    </form>
</div>

<script src="scripts.js"></script>

<?php
if ( !empty( $_POST["email"] ) && !empty( $_POST["firstName"] ) ) {
    echo "<script>document.getElementById('success').className = 'active';</script>";
}
include 'footer.php';
?>

</body>
</html>