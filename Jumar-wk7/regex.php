<?php
$name = "Jumar";
$name = scrubInput($name);
$nameErr = "";

if ( !preg_match( "/^[a-zA-Z \'\-]*$/", $name ) ) {
    $nameErr = "Only letters and white space allowed";
} else {
    $nameErr = "Ok<br>";
}

echo $name." ".$nameErr;


function scrubInput($data) {
    $data = trim( $data );
    $data = stripslashes( $data );
    $data = htmlspecialchars( $data );
    return $data;
}
?>