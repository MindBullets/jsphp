<?php
$name = $email = $subject = $message = $nameErr = $emailErr = $subjectErr = $messageErr = $errors = '';
function validateForm() {
    global $name, $email, $subject, $message, $nameErr, $emailErr, $subjectErr, $messageErr, $errors;

    // validate all input fields
    if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {

        //firstName
        if ( empty( $_POST["firstName"] ) ) {
            $nameErr = "First name is required";
            $name = $_POST["firstName"];
            // $errors = true;
        } else {
            $name = scrubInput( $_POST["firstName"] );
            if ( !preg_match( "/^[a-zA-Z \'\-]*$/", $name ) ) {
                $nameErr = "Only letters, whitespace, apostrophe (') and dash (-) allowed";
                // $errors = true; // do we need this?
                $name = $_POST["firstName"];
            } else {
                $nameErr = false;
                $name = $_POST["firstName"];
            }
        }

        //email
        if ( !empty( $_POST["email"] ) && filter_var( $_POST["email"], FILTER_VALIDATE_EMAIL ) === false ) {
            $emailErr = "Invalid email";
            $email = $_POST["email"];
        } else if (empty( $_POST["email"] ) ) {
            $emailErr = "Please enter an email";
            $email = $_POST["email"];
        } else {
            $emailErr = false;
            $email = $_POST["email"];
        }

        //subject
        if ( !empty( $_POST["subject"] ) ) {
            $subject = $_POST["subject"];
            $subjectErr = false;
        } else {
            $subjectErr = "Please enter a subject";
        }

        //message
        if ( !empty( $_POST["message"] ) ) {
            $message = $_POST["message"];
            $messageErr = false;
        } else {
            $messageErr = "Please enter a message";
        }

        if ( !( $nameErr && $emailErr && $subjectErr && $messageErr ) ) {
            return true;
        }
    }
}

function scrubInput($data) {
    $data = trim( $data );
    $data = stripslashes( $data );
    $data = htmlspecialchars( $data );
    return $data;
}

//sticky add field
function listAddedFields() {
    if ( isset( $_POST['breed'] ) ) {
        $bCount = 0;
        $builtList = "";
        $len = count( $_POST['breed'] );
        $breedsArr = $_POST['breed'];

        for ($i=0; $i < $len; $i++) {
            if ( $breedsArr[$i] != "" ) {
                $builtList .= '<li><label for="breed' .$bCount. '">Breed ' .($bCount+1). ':</label><input type="text" name="breed[] id="breed'.$bCount.'" value="'.$breedsArr[$i].'">';
                $bCount++;
            }
        }
    } else {
        $builtList = "";
    }
    return $builtList;
}

function sendMyMsg( $msg ) {
    global $email, $subject;
    $headers = "MIME-Version: 1.0"."\r\n";
    $headers .= "Content-type/text/html;charset=UTF-8"."\r\n";
    $headers .= "From: jbalacy@gmail.com";
    $toEmail = "jbalacy@gmail.com, $email";

    mail( $toEmail, $subject, $msg, $headers);
}