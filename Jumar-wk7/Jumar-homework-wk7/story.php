<?php
    $title = "Home";
    include 'header.php';
?>
    <section class="clearfix" id="story">
        <div class="story-textarea">
            <h1>French Bulldogs aka Frenchies</h1>
            <p>Frenchies are a mix of English Bulldogs, Boston Terriers and Pugs. They became popular when lace workers from England settled in France. </p>
            <p>Frenchie past times include napping, eating treats and getting belly rubs.</p>
        </div>
    </section>
<?php
    include 'footer.php';
?>

</body>
</html>