<?php
// include 'validate.php';
// if ( validateForm() ) {
//     echo "<script>document.getElementById('success').className = 'active';</script>";
// }
// validateForm();

$title = "Home";
include 'header.php';

include 'validate.php';
// if ( validateForm() ) {
//     echo '<script>document.getElementById("success").className="active";</script>';
// }
?>

<h1>Contact Me</h1>
<div id="success" class="inactive">Your message has been sent!</div>
<?php
if ( validateForm() ) {
    echo '<script>document.getElementById("success").className="active";</script>';
}
?>
<form name="contact" novalidate action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
    <label for="firstName">First Name <span class="red">*</span></label>
    <small class="error"><?php echo $nameErr ?></small>
    <input type="text" id="firstName" name="firstName" value="<?php echo $name; ?>" autofocus>

    <label for="email">E-mail <span class="red">*</span></label>
    <small class="error"><?php echo $emailErr ?></small>
    <input type="email" name="email" id="email" value="<?php echo $email; ?>" required>

    <label for="subject">Subject <span class="red">*</span></label>
    <small class="error"><?php echo $subjectErr ?></small>
    <input type="text" name="subject" id="subject" value="<?php echo $subject; ?>" required>

    <label for="message">Message <span class="red">*</span></label>
    <small class="error"><?php echo $messageErr ?></small>
    <textarea name="message" id="message" required><?php echo $message; ?></textarea>
    <label for="extra">Want to talk about different breeds?</label>
    <ul id="extra">
        <!-- create extra li in here onclick -->
        <?php echo listAddedFields(); ?>
    </ul>
    <button type="button" class="add" onclick="buildExtraField()">Add breeds</button>
    <br><br>
    <div>
        <input type="checkbox" onclick="iAgree()" id="agreeCheckbox" value="awesome">I agree that this is awesome<br>
    </div>
    <input type="button" onclick="clearForm(this)" value="Clear Form" >&nbsp; &nbsp;
    <input type="submit" id="submit" name="Submit" value="Send Form"  disabled>
</form>

<script src="scripts.js"></script>

<?php
if ( !empty( $_POST["email"] ) && !empty( $_POST["firstName"] ) ) {
    echo "<script>document.getElementById('success').className = 'active';</script>";
}
include 'footer.php';
?>

</body>
</html>