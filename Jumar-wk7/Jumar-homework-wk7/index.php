<?php
    $title = "Home";
    include 'header.php';
?>
<main>
    <section class="hero-homepage clearfix">
        <div class="wrapper">
            <h1 class="title">
                Hello, <br>
                my name is Katsu!
            </h1>
            <a href="#photos" class="button button-primary-color">See my photos</a>
        </div>
    </section>
    <section class="photo-list-homepage clearfix" id="photos">
        <h2>My Photos</h2>
        <!-- Photo 1-->
        <figure class="card">
            <img src="images/Katsu-001-550x550.jpg" alt="French bulldog on a beanbag." aria-describedby="katsu-beanbag">
            <figcaption id="katsu-beanbag" class="description">
                <h3>Me on a beanbag</h3>
                <p>My humans bought this beanbag before I was born, but I've claimed it as my new favorite bed. MWAHAHAHAH!</p>
                <div class="card-footer">
                    <a href="images/Katsu-001.jpg" class="button button-primary-color button-small">download this photo</a>
                </div>
                <p>Photo credit: Jumar Balacy</p>
            </figcaption>
        </figure>

        <!-- Photo 2-->
        <figure class="card">
            <img src="images/Katsu-005-550x550.jpg" alt="French bulldog on a couch." aria-describedby="katsu-couch">
            <figcaption id="katsu-couch" class="description">
                <h3>Me on a couch</h3>
                <p>I like to lie down on the couch. I own this couch and whenever someone sits here I sit on their lap.</p>
                <div class="card-footer">
                    <a href="images/Katsu-005.jpg" class="button button-primary-color button-small">download this photo</a>
                </div>
                <p>Photo credit: Jumar Balacy</p>
            </figcaption>
        </figure>

        <!-- Photo 3-->
        <figure class="card">
            <img src="images/Katsu-bed-550x550.jpg" alt="French bulldog on a beanbag." aria-describedby="katsu-bed">
            <figcaption id="katsu-bed" class="description">
                <h3>Me on my bed</h3>
                <p>This is supposed to be my bed, but I only like to lie half on it.</p>
                <div class="card-footer">
                    <a href="images/Katsu-bed.jpg" class="button button-primary-color button-small">download this photo</a>
                </div>
                <p>Photo credit: Jumar Balacy</p>
            </figcaption>
        </figure>

        <!-- Photo 4-->
        <figure class="card">
            <img src="images/Katsu-happySleep-550x550.jpg" alt="French bulldog smiling while sleeping." aria-describedby="katsu-happy-sleep">
            <figcaption id="katsu-happy-sleep" class="description">
                <h3>Every nap</h3>
                <p>This is every nap</p>
                <div class="card-footer">
                    <a href="images/Katsu-happySleep.jpg" class="button button-primary-color button-small">download this photo</a>
                </div>
                <p>Photo credit: Jumar Balacy</p>
            </figcaption>
        </figure>

        <!-- Photo 5-->
        <figure class="card">
            <img src="images/Katsu-travel-550x550.jpg" alt="French bulldog soft carrier." aria-describedby="katsu-travel">
            <figcaption id="katsu-travel" class="description">
                <h3>Jetsetter, no big deal</h3>
                <p>Sometimes I like to fly. Even TSA loves me.</p>
                <div class="card-footer">
                    <a href="images/Katsu-travel.jpg" class="button button-primary-color button-small">download this photo</a>
                </div>
                <p>Photo credit: Jumar Balacy</p>
            </figcaption>
        </figure>

        <!-- Photo 6-->
        <figure class="card">
            <img src="images/Katsu-work-550x550.jpg" alt="French bulldog on a beanbag." aria-describedby="katsu-work">
            <figcaption id="katsu-work" class="description">
                <h3>Time to work</h3>
                <p>Sometimes I help my parents with their work. They need lots of help.</p>
                <div class="card-footer">
                    <a href="images/Katsu-work.jpg" class="button button-primary-color button-small">download this photo</a>
                </div>
                <p>Photo credit: Jumar Balacy</p>
            </figcaption>
        </figure>
    </section>

</main>
<?php
    include 'footer.php';
?>

</body>
</html>