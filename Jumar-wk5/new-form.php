<!DOCTYPE html>
<html lang="en">
<head>
    <title>Form</title><meta charset="UTF-8">
    <style>
        div { width: 20rem; border: solid 2px #acacac; padding: 2rem; }
    </style>
</head>
<body>
    <?php
    if (isset($_POST['email'])) {
        foreach ($_POST as $key => $value) {
            echo "$key : $value<br>";
        }
    }

    ?>

    <div><h2 style = "text-align:center">Contact Me</h2>
    <form name="contact" novalidate action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
        <p>Your Name: <br>
        <input type="text" name="firstName" ></p>

        <p>Your E-mail: <br>
        <input type="email" name="email" required></p>

        <p>Subject: <br>
        <input type="text" name="Subject" /></p>

        <p>Message:<br >
        <textarea name="message"></textarea></p>

        <p><input type="button" value="Clear Form" >&nbsp; &nbsp;
        <input type="submit" name="Submit" value="Send Form" ></p>
    </form>
  </div>
</body>
</html>
