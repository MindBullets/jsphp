<?php
/* syntax is
$my_array = array(values); */

$Pool = array( 'white', 'yellow', 'blue', 'red', 'violet', 'orange' );

echo $Pool[1];

echo '<h1>print_r()</h1>';
print_r($Pool);

echo '<h1>var_dump()</h1>';
var_dump($Pool);

echo '<h1>var_export()</h1>';
var_export($Pool);
?>