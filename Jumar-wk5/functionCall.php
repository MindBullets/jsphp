<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function call</title>
</head>
<body>
<?php
if (isset($_POST['testWord'])) {
    echo check_palindrome($_POST['testWord']);
}

function check_palindrome($string) {
    if ($string == strrev($string)) {
        return 'yes';
    } else {
        return 'no';
    }
}
?>

<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
<label for="testWord">Input a test word</label>
<input type="text" name="testWord" value="">
<br>
<br>
<br>
<input type="submit" value="Register">

</form>
</body>
</html>