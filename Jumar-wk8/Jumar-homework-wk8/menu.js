function customMenu() {
    var allMenuItems = document.querySelectorAll(".menu");
    //grab url for comparison
    var myUrl = window.location.href;
    var foundIt = false;

    for ( let i = 0; i < allMenuItems.length; i++ ) {
        var finder = myUrl.indexOf(allMenuItems[i].href);
        if ( finder > -1 ) {
            foundIt = true;
            document.getElementById(allMenuItems[i].id).style.color = "#000";
            document.getElementById(allMenuItems[i].id).style.backgroundColor = "#978d8d";

        } else {
            document.getElementById(allMenuItems[i].id).style.color = "#FFF";
        }
    }
    if ( foundIt == false ) {
        document.getElementById("index").style.color = "#000";
        document.getElementById("index").style.backgroundColor = "#978d8d";

    }
}