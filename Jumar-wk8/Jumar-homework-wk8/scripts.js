var agreeCheckboxChecked = false;



function getInputType(input) {
	return input.type;
}

function clearForm(myInput){

    let myForm = myInput.form;
    //   document.test1.reset();
    //#1 add explicit clears here as needed
    // make sure you check in multiple browsers and don't forget the error message

    for (let i = 0; i < myForm.length; i++) {
        switch (getInputType(myForm[i])) {
            case "text":
            case "email":
            case "textarea":
                myForm[i].value = "";
                break;
            case "select-one":
                myForm[i].selectedIndex = 0;
                break;
            case "checkbox":
                myForm[i].checked = false;
                break;
            case "submit":
                //#9 disable the submit since you probably cleared the "I agree" box.
                myForm[i].disabled = true;
                break;
        }
    }
    document.getElementById("extra").innerHTML = "";
    //when you clear a form you want to start from the beginning like the page load
    document.getElementById("firstName").focus();
    // document.getElementById("confirmMsg").innerHTML = "";
    count = 0;
}

// function startHere() {
//     document.getElementById("firstName").focus();
//     document.getElementById("submit").disabled = true;
// }

function iAgree() {
    var submitButton = document.getElementById("submit");

    if (document.getElementById("agreeCheckbox").checked) {
        submitButton.disabled = false;
        submitButton.setAttribute("style", "background-color: #5B8930;color: #ebf6f7;");
        agreeCheckboxChecked = true;
    } else {
        submitButton.setAttribute("style", "background-color: #dadada;");
        submitButton.disabled = true;
    }
}

function buildExtraField() {
    /* count the number of children of a certain type getElementByid("extra")
    use that for count */
    var container = document.getElementById("extra");

    var count = container.getElementsByTagName("LI").length + 1;
    var newId = "breed" + count;
    var newElem = document.createElement("li");
    newElem.innerHTML = '<label for="' +newId+ '">Breed ' + count + ':</label><input type="text" name="breed[]" id="'+newId+'" value=""><span class="error"></span><br>';

    container.appendChild(newElem);
    document.getElementById(newId).focus();
}

function checkForm() {
    //#6 fix this conditional to test firstName and email. Don't allow empty inputs
    if( document.forms.contact.firstName.value && document.forms.contact.email.value && agreeCheckboxChecked ) {
        alert("Success");
        return true;
    } else {
        alert("Please fill in all the fields");
        return false;
    }
}